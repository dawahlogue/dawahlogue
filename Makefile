modules:
	@echo "Updating modules..."
	./internal/scripts/update_modules.sh

serve: modules
	hugo server -D

build: modules
	hugo --gc --minify

clean:
	@rm -rf public
	@rm -rf static/modules

diagrams:
	./internal/scripts/update_diagrams.sh

.PHONY: modules serve build clean diagrams
