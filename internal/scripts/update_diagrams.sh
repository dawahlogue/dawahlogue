#!/usr/bin/env bash

cd internal/diagrams
for diagram_source in *.d2;
do
	diagram_target="$(basename -s .d2 $diagram_source).svg"
	d2 $diagram_source $diagram_target
done
