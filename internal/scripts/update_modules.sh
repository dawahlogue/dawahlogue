#!/usr/bin/env bash

rm static/modules -rf
mkdir static/modules -p

sed -i '/^# Modules$/,$d' content/_index.md
echo -e "# Modules\n" >> content/_index.md

for module_path in $(ls internal/modules/*);
do
	module_key="$(basename -s .twee "$module_path")"
	module_title="$(head -n 2 "$module_path" | tail -1)"

	mkdir "static/modules/$module_key"

	tweego "$module_path" internal/stylesheet.twee -o "static/modules/$module_key/index.html"
	echo "- [$module_title](/modules/$module_key)" >> content/_index.md
done
