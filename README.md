# Introduction

dawahlogue is a website containing arguments for Islām as interactive conversations.

It is hosted at [dawahlogue.gitlab.io](https://dawahlogue.gitlab.io).

# Purpose

There are many certain doubts which have fixed answers depending on the person's existing beliefs. However, this also means that one answer can't be given to everyone as it would either (i) not be addressing the person's beliefs or (ii) be way too long.

dawahlogue aims to counter this by presenting arguments as interactive conversations using dialogue trees. Some diagrams are given below. Note that the dialogue tree software used (see `Contributing->With Content` for details) is much more powerful than demonestrated in the diagrams.

![Diagram 1](https://gitlab.com/dawahlogue/dawahlogue.gitlab.io/-/raw/main/internal/diagrams/1.svg)
![Diagram 2](https://gitlab.com/dawahlogue/dawahlogue.gitlab.io/-/raw/main/internal/diagrams/2.svg)

# Contributing

## With Content

The site's main content lives inside `internal/modules` as a set of `.twee2` files. These files are obtained by exporting a story from [Twinery](https://twinery.org/v2). The content is managed using git, and contributions are through merge requests. Thus, it is highly recommended to learn the basics of git if you plan on contributing. You can also use a git GUI like [Git Cola](https://git-cola.github.io/).

Nevertheless, there is a way to contribute using just GitLab's web interface.

To add new modules:
- Use [Twinery](https://twinery.org/2) to create a story.
- Go to Build -> Export as Twee.
- Rename the exported file to `[slug].twee2` (`[slug]` can be something like `the-argument-from-dependency`) and upload it to [internal/modules](https://gitlab.com/dawahlogue/dawahlogue.gitlab.io/-/tree/main/internal/modules).
- Submit a merge request (Make sure to prefix the commit message with `[content]`).

To edit existing modules:
- Download the `[slug].twee2` file from `internal/modules`.
- In Twinery, go to `Library -> Import` and import the file.
- The rest of the steps are the same as for adding new modules.

Useful links:
- [Video guides on the dawahlogue channel](https://youtube.com/@dawahlogue)
- [IJMES Transliteration System](https://muslimphilosophy.org/ijmests)
- [Twinery Cookbook](https://twinery.org/cookbook)
- [Twinery Reference](https://twinery.org/reference/en)

## With Code

This is a normal [Hugo](https://gohugo.io) site with some pre processing.

A Makefile is included as a wrapper for convenience.

If you made additions/changes to the diagrams, make sure to run `make diagrams` to render the diagrams to svg. You will need [d2](https://d2lang.com/) installed.

If you face an error saying that a certain story format is not available, you can download it from [the twinejs repository on GitHub](https://github.com/klembot/twinejs/tree/develop/public/story-formats).

# Install Instructions

## Build Dependencies

- `hugo`
- `tweego`
- `make` (Optional, you can simply run the commands from Makefile yourself.)

## Building

Clone the source code:

```shell
git clone https://gitlab.com/dawahlogue/dawahlogue.gitlab.io
cd dawahlogue.gitlab.io
```

Build:

```shell
make build
```

# Deploying

Simply copy the `public/` folder and serve it using something like nginx/caddy.
